import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { from } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';

/** Componentes */
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './pages/home.component';
import { GiphyComponent } from './pages/giphy.component';
import { PreguntasComponent } from './pages/preguntas.component';


/** Servicios */
import { GiphyService } from './services/giphy.service';


/** Rutas */
import { APP_ROUTING } from './app.root';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    GiphyComponent,
    PreguntasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    APP_ROUTING
  ],
  providers: [GiphyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
