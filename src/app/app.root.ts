import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home.component';
import { GiphyComponent } from './pages/giphy.component';
import { PreguntasComponent } from './pages/preguntas.component';

/** Control de rutas */
const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'giphy', component: GiphyComponent },
  { path: 'preguntas', component: PreguntasComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home'},
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
