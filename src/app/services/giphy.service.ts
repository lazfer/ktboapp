import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GiphyService {

  constructor(private http:HttpClient){
  }

  /** Endpoint del servicio */
private endpoint = 'http://api.giphy.com/v1/gifs/search?api_key=dc6zaTOxFJmzC&q=';

/** Funcion que obtiene las imagenes */
  getGiphy(valsearch){
    return this.http.get(this.endpoint+valsearch);
}
}
