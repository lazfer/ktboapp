import { Component, OnInit } from '@angular/core';
import { GiphyService } from '../services/giphy.service';

@Component({
  selector: 'app-giphy',
  templateUrl: './giphy.component.html',
  styleUrls: ['./giphy.component.css']
})
export class GiphyComponent implements OnInit{

  /** Guarda la respuesta del servicio */
  imagenes: any[] = [];

constructor(private _giphyService:GiphyService){
}

/** Ejecuta la busqueda de las imagenes */

buscar(img){
  this._giphyService.getGiphy(img)
  .subscribe( (items:any) => {
       this.imagenes = items.data;
  });

}

ngOnInit() {

}
}
