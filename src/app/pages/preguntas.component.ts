import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preguntas',
  templateUrl: './preguntas.component.html',
  styleUrls: ['./preguntas.component.css']
})
export class PreguntasComponent implements OnInit{

constructor(){

}
res = '';

ngOnInit() {

  /** Guarda la respuesta en una variable local */
  this.res = 'const arr = [{id: 1, name: "mapache"}, {id:2, name:"perrito" }]; ' +
         ' arr.map(item => { '+
          ' if(item.name == "mapache"){ '+
          ' console.log(item.id); '+
          ' } '+
          ' }); '
}

}
