# Kbtoap

 Proyecto creado en Angular version 6.x
 Bootstrap 4.x
 
## Instalacón previa
nodejs versión estable
npm
npm install -g @angular/cli versión estable 
git 

## Clonar proyecto 
utilizar git clone

url https://gitlab.com/lazfer/ktboapp.git

## Iniciar aplicación 

npm install

## Correr aplicación

ng serve -o 

## Abrir en el navegado

http://localhost:4200/ 
